package experiments;

import java.lang.reflect.*;

/******************************************************************************
 * Not only can the value of a private field be accessed, but this experiment
 * shows that it can be altered!!
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2017
 ******************************************************************************/

public class Reflection08 {
  public static void main(String[] args) throws Exception {
	  ExperimentMain s = new ExperimentMain();
    Field[] fields = s.getClass().getDeclaredFields();
    System.out.printf("There are %d fields\n", fields.length);
    for (Field f : fields) {
      f.setAccessible(true);
      float x = f.getFloat(s);
      x++;
      f.setFloat(s, x);
      System.out.printf("field name=%s type=%s value=%.2f\n", f.getName(),
          f.getType(), f.getFloat(s));
    }
  }
}
