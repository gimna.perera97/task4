package kunitDemo1;
import static kunit1.KUnit.*;

/******************************************************************************
 * This code demonstrates the use of the KUnit testing tool. It produces a
 * report that contains messages generated from the check methods.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class TestSimple {

  void checkConstructorAndAccess(){
    Simple s = new Simple(7, 8);
    checkEquals(s.getA(), 8);
    checkEquals(s.getB(), 8);
    checkEquals(s.getB(), 8);    
    checkEquals(s.getB(), 9);    
  }



void checkSquareA(){
    Simple s = new Simple(7, 8);
    s.squareA();
    checkEquals(s.getA(), 13);
  }

  public static void main(String[] args) {
    TestSimple ts = new TestSimple();
    ts.checkConstructorAndAccess();
    ts.checkSquareA();
    report();
  }
}
