package experiments;

public class ExperimentMain {
	public float a = (float) 15.73;
	private float b = (float) 22.54;

	  public ExperimentMain() {
	  
	  }

	  public void squareA() {
	    this.a *= this.a;
	  }

	  private void squareB() {
	    this.b *= this.b;
	  }

	  public float getA() {
	    return a;
	  }

	  private void setA(float a) {
	    this.a = a;
	  }

	  public float getB() {
	    return b;
	  }

	  public void setB(float b) {
	    this.b = b;
	  }
	  public String toString () { 
		  return String.format (" (Number1: % f, Number2: % f)" , a , b ) ;  
	  } 
	  public double getC ( ) { 
		  return a;
	  }
	  
	  private void setc (float c) { 
		  this.a = c;
		  } 
	  public double getD ( ) { 
		  return b ;  
		  } 
	  public void setD (float d ) { 
		  this.b= d ; 
		  }

}
